<?php




header('Content-Type: text/html; charset=UTF-8');

session_start();


if (!empty($_SESSION['login'])) {
  session_destroy();
  header('Location: index.php');
}


if ($_SERVER['REQUEST_METHOD'] == 'GET') {
if (!empty($_GET['no_such_login']))
print("<div>Пользователь с таким логином отсутствует в базе данных</div>");
if (!empty($_GET['wrong_pass']))
print("<div>Кажется, вы ввели неверный пароль</div>");

?>
<form action="" method="post">
  <input name="login" />
  <input name="pass" />
  <input type="submit" value="Войти" />
</form>
<?php
}

else {

  $conn = new PDO("mysql:host=localhost;dbname=u40980", 'u40980', '1404971', array(PDO::ATTR_PERSISTENT => true));



  $user = $conn->prepare('SELECT id, u_pass FROM form WHERE u_login = ?'); 
  $user -> execute([$_POST['login']]);
  $row = $user->fetch(PDO::FETCH_ASSOC);

  if(!$row){
    header('Location: ?no_such_login=1');
    exit();
  }

  if(md5($_POST['pass']) != $row['u_pass']){
    header('Location:?wrong_pass=1');
    exit();
  }
      


  $_SESSION['login'] = $_POST['login'];

  $_SESSION['uid'] = $row['id'];
  $errors['pass'] = false;
  $errors['login'] = false;

  header('Location: index.php');
}
